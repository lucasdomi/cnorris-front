[![Netlify Status](https://api.netlify.com/api/v1/badges/4d70c20e-1800-4724-8cc0-7db82af2ee92/deploy-status)](https://app.netlify.com/sites/quirky-allen-3fa1c4/deploys)

# Chuck Norris Front

[Comentários sobre o projeto](HISTORY.md)

### Rodar o projeto

Na pasta raiz do projeto, digite os seguintes comandos para rodar o projeto

```
npm install
npm start
```

ou

```
yarn
yarn start
```

Depois, é necessário acessar `http://localhost:3000/`

####Rodar os testes

Para rodar os testes automazidos, o comando necessário é:

```
npm run test
```

ou

```
yarn test
```
