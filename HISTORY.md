### Dia 1

Após a leitura do desafio, resolvi pensar sobre a estrutura que irei utilizar para realiza-lo. Utilizarei o boilerplate do react para iniciar o projeto. Irei utilizar React com redux para controlar o estado da aplicação e styled-components para o css. Para ajudar a organizar o código, eslint com o prettier.

### Dia 2

No 2o dia que passou após receber o desafio, resolvi montar a estrutura do fluxo que imaginei do React + Redux, ainda sem estilizar nada. Deixei a aplicação rodando em funcionamento, mas com muitos desafios ainda pela frente.

### Dia 3

Estilização das páginas

### Dia 4

Trocando algumas lógicas para guardar as categorias no estado e evitar realizar uma nova requisição desnecessária. Ajustes no template e adicionando a função para trocar de texto com a seta do teclado. Inclui testes no cypress para testar o funcionamento dos botões.

### Dia 5

Refatorações no código e inclusão de um debounce para melhorar a perfomance caso a pessoa troque o texto na seta.
