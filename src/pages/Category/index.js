import React, { useEffect, useState } from 'react';
import debounce from 'lodash.debounce';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import api from '../../services/api';
import * as CategoryActions from '../../store/modules/category/actions';
import * as S from './styled';
import Loading from '../../components/Loading';
import Image from '../../assets/chuck-norris-loading.png';

function Category({ match, dispatch, loading, history, loadingCategory }) {
  const [value, setValue] = useState('');
  const [created, setCreated] = useState('');

  useEffect(() => {
    const actionData = async () => {
      await dispatch(CategoryActions.loadingCategory(true));

      const response = await api.get(
        `random?category=${match.params.category}`
      );

      setValue(response.data.value);
      setCreated(response.data.created_at);

      await dispatch(CategoryActions.loadingCategory(false));
    };

    function loadData() {
      actionData();
    }

    const handleKeyPress = e => {
      if (e.keyCode === 39) {
        actionData();
      }
    };

    function setFromEvent(e) {
      const callDebounce = debounce(() => handleKeyPress(e), 300);
      callDebounce();
    }

    loadData();
    window.addEventListener('keydown', setFromEvent);
    return () => {
      window.removeEventListener('keydown', setFromEvent);
    };
  }, [dispatch, loadingCategory, match.params.category]);

  async function handleClick() {
    await dispatch(CategoryActions.loadingCategory(true));
    const response = await api.get(`random?category=${match.params.category}`);
    setValue(response.data.value);
    setCreated(response.data.created_at);
    await dispatch(CategoryActions.loadingCategory(false));
  }

  async function handleClickHome() {
    history.push(`/`);
  }

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <S.Wrapper data-cy="wrapper">
          <img alt="teste" src={Image} width="150" height="150" />

          <S.Message>
            <p>{value}</p>
          </S.Message>
          <S.Date>Created {created.substr(0, 10)}</S.Date>

          <S.Text>Press the => key or the next button to change</S.Text>

          <S.ActionButtons>
            <S.Button
              data-cy="back"
              type="button"
              onClick={() => handleClickHome()}
            >
              Back to categories
            </S.Button>
            <S.Button
              data-cy="next"
              type="button"
              onClick={() => handleClick()}
            >
              Next
            </S.Button>
          </S.ActionButtons>
        </S.Wrapper>
      )}
    </>
  );
}

Category.defaultProps = {
  loadingCategory: () => {},
};

Category.propTypes = {
  loading: PropTypes.bool.isRequired,
  loadingCategory: PropTypes.func,
  match: PropTypes.oneOfType([PropTypes.object]).isRequired,
  history: PropTypes.oneOfType([PropTypes.object]).isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect(state => ({
  loading: state.category.loading,
}))(Category);
