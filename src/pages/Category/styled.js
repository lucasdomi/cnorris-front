import styled from 'styled-components';
import { lighten } from 'polished';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 90%;
`;

export const Message = styled.blockquote`
  background: #f9f9f9;
  border-left: 10px solid #ccc;
  margin: 0.7em 10px;
  padding: 0.5em 10px;
  font-style: italic;
  &:before {
    color: #ccc;
    content: open-quote;
    font-size: 4em;
    line-height: 0.1em;
    margin-right: 0.25em;
    vertical-align: -0.4em;
  }

  p {
    display: inline;
  }
`;

export const Date = styled.span`
  font-size: 12px;
  margin-bottom: 20px;
  color: #e8e8e8;
`;

export const ActionButtons = styled.div`
  display: flex;
`;

export const Button = styled.button`
  background: #3498db;
  border: 1px solid #d8d8d8;
  display: block;
  color: #e8e8e8;
  font-size: 17px;
  margin-bottom: 2em;
  padding: 10px;
  white-space: normal;
  margin: 5px;

  &:hover {
    background-color: ${lighten(0.1, '#3498db')};
  }
`;

export const Text = styled.span`
  font-size: 14px;
  margin-bottom: 10px;
  color: #e8e8e8;
`;
