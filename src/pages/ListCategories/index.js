import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import api from '../../services/api';
import * as ListCategoryActions from '../../store/modules/list-category/actions';
import * as S from './styled';
import Image from '../../assets/chuck-norris-loading.png';
import Loading from '../../components/Loading';

function ListCategories({
  history,
  dispatch,
  loading,
  loadingListCategory,
  categories,
}) {
  useEffect(() => {
    async function loadData() {
      if (categories.length === 0) {
        await dispatch(ListCategoryActions.loadingListCategory(true));
        const response = await api.get('categories');
        await dispatch(ListCategoryActions.addCategories(response.data));
        await dispatch(ListCategoryActions.loadingListCategory(false));
      }
    }
    loadData();
  }, [categories, dispatch, loadingListCategory]);

  const handleClick = category => {
    history.push(`/category/${category}`);
  };

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          <S.Header>
            {/* <img alt="teste" src={Image} width="150" height="150" /> */}
            <S.Title>Choose your joke category</S.Title>
          </S.Header>

          <S.MainWrapper>
            {categories &&
              categories.map(category => (
                <S.Button
                  data-cy="submit"
                  image={Image}
                  type="button"
                  key={category}
                  onClick={() => handleClick(category)}
                >
                  {category}
                </S.Button>
              ))}
          </S.MainWrapper>
        </>
      )}
    </>
  );
}

ListCategories.defaultProps = {
  loadingListCategory: () => {},
};

ListCategories.propTypes = {
  loading: PropTypes.bool.isRequired,
  loadingListCategory: PropTypes.func,
  categories: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string]))
    .isRequired,
  history: PropTypes.oneOfType([PropTypes.object]).isRequired,
  dispatch: PropTypes.func.isRequired,
};

export default connect(state => ({
  loading: state.listCategory.loading,
  categories: state.listCategory.categories,
}))(ListCategories);
