import styled from 'styled-components';
import media from 'styled-media-query';
import { lighten } from 'polished';

export const MainWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, auto));
  height: calc(100% - (140px + 50px));

  ${media.lessThan('small')`
    display: grid;
    grid-template-columns: 1fr;
  `}
`;

export const Header = styled.header`
  display: flex;
  width: 100%;
  height: 150px;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const Button = styled.button`
  background: #3498db;
  border: 1px solid #d8d8d8;
  display: block;
  color: #e8e8e8;
  font-size: 17px;
  margin-bottom: 2em;
  padding: 1em;
  white-space: normal;
  margin: 20px;
  transition: width 1.5s ease;

  &:hover {
    background-color: ${lighten(0.1, '#3498db')};
    cursor: url('http://cur.cursors-4u.net/people/peo-6/peo696.cur'), auto !important;
  }

  ${media.lessThan('small')`
    margin: 5px;
  `};
`;

export const Title = styled.h2`
  color: #e8e8e8;
  /* margin-top: 20px; */
`;
