import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import ListCategories from './pages/ListCategories';
import Category from './pages/Category';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={ListCategories} />
        <Route path="/category/:category" component={Category} />
      </Switch>
    </BrowserRouter>
  );
}
