import React from 'react';
import ClipLoader from 'react-spinners/RotateLoader';
import { css } from '@emotion/core';
import { Wrapper } from './styled';

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
  top: 42%;
  position: fixed;
  left: 48%;
`;

const mobileOverride = css`
  display: block;
  margin: 0 auto;
  border-color: red;
  top: 48%;
  position: fixed;
  left: 45%;
`;

export default function Loading() {
  return (
    <Wrapper>
      <ClipLoader
        css={window.innerWidth > 768 ? override : mobileOverride}
        size={30}
        color="#3498db"
        loading
      />
    </Wrapper>
  );
}
