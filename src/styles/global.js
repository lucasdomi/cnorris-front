import { createGlobalStyle } from 'styled-components';
import stars from '../assets/stars.gif';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  *:focus {
    outline: 0;
  }

  html, body, #root {
    height: 100vh;
    background: url(${stars})
  }

  body {
    -webkit-font-smoothing: antialiased;
    background: #cccccc;
  }

  body, input, button {
    font-family: Arial, Helvetica, sans-serif;
  }

  a {
    text-decoration: none
  }

  ul {
    list-style: none;
  }

  button {
    cursor: pointer;
  }
`;
