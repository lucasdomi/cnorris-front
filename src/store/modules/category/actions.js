export function loadingCategory(loading) {
  return {
    type: 'LOADING_PAGE_CATEGORY',
    loading,
  };
}
