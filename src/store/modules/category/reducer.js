const initialState = {
  loading: true,
};

export default function category(state = initialState, action) {
  switch (action.type) {
    case 'LOADING_PAGE_CATEGORY':
      return { ...state, loading: action.loading };
    default:
      return state;
  }
}
