export function loadingListCategory(loading) {
  return {
    type: 'LOADING_PAGE_LISTCATEGORY',
    loading,
  };
}

export function addCategories(categories) {
  return {
    type: 'CHANGE_CATEGORIES',
    categories,
  };
}
