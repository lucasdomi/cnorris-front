export const initialState = {
  loading: true,
  categories: [],
};

export default function category(state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_CATEGORIES':
      return { ...state, categories: action.categories };
    case 'LOADING_PAGE_LISTCATEGORY':
      return { ...state, loading: action.loading };
    default:
      return state;
  }
}
