import { combineReducers } from 'redux';

import category from './category/reducer';
import listCategory from './list-category/reducer';

export default combineReducers({ category, listCategory });
