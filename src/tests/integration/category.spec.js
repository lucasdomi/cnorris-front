describe('My first test', () => {
  beforeEach(() => {
    cy.visit('/category/animal');
  });

  it('Test click one button', () => {
    cy.get('[data-cy=next]')
      .first()
      .click();
  });

  it('Test click arrow', () => {
    cy.get('body').type('{rightarrow}');
  });

  it('Test click back button', () => {
    cy.get('[data-cy=back]')
      .first()
      .click();
  });
});
