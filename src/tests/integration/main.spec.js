describe('My first test', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('Test click one button', () => {
    cy.get('[data-cy=submit]')
      .first()
      .click();
  });
});
